package ru.kombarov.tm;

import java.util.ArrayList;

public class TaskManager {
    ArrayList<String> tasklist = new ArrayList<String>();

    public void taskClear()
    {
        tasklist.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void taskCreate(String str)
    {
        tasklist.add(str);
        System.out.println("OK");
    }

    public void taskList()
    {
        for (int i=0; i<tasklist.size();i++)
        {
            System.out.println(i+1 + ". " + tasklist.get(i));
        }
    }

    public void taskRemove(String str)
    {
        tasklist.remove(str);
        System.out.println("OK");
    }
}
