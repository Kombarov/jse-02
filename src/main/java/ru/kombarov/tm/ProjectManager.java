package ru.kombarov.tm;

import java.util.ArrayList;

public class ProjectManager {
    ArrayList<String> projectlist = new ArrayList<String>();

    public void projectClear()
    {
        projectlist.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void projectCreate(String str)
    {
        projectlist.add(str);
        System.out.println("OK");
    }

    public void projectList()
    {
        for (int i=0; i<projectlist.size();i++)
        {
            System.out.println(i+1 + ". " + projectlist.get(i));
        }
    }

    public void projectRemove(String str)
    {
        projectlist.remove(str);
        System.out.println("OK");
    }
}
